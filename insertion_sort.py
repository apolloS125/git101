def selection_sort(numbers):
    n = len(numbers)
   
    for i in range(n):
        
        min_index = i
        for j in range(i + 1, n):
            if numbers[j] < numbers[min_index]:
                min_index = j

       
        numbers[i], numbers[min_index] = numbers[min_index], numbers[i]

    return numbers

if __name__ == "__main__":
    numbers = list(map(int, input("Enter integer numbers separated by space: ").split()))
    sorted_numbers = selection_sort(numbers.copy())  # Using .copy() to avoid modifying the original list
    print("Sorted numbers are:", sorted_numbers)
